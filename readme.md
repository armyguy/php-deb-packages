###This repository contains example for preparing deb packages

* php7.0-memcached
* php7.0-rar

```shell
$ dpkg-deb --build php_memcached/php7.0-memcached
$ cd php_memcached
$ dpkg -i php7.0-memcached.deb
$ dpkg -P php7.0-memcached
```

Also this deb files include postinstallation and preremove instructions
